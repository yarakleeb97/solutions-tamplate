$("#clients").owlCarousel({
  loop: true,
  autoplay: true,
  margin: 10,
  nav: true,
  items: 1,
  navText: [
    "<i class='bi bi-caret-left'></i>",
    "<i class='bi bi-caret-right'></i>",
  ],
});
$("#brands").owlCarousel({
  loop: true,
  autoplay: true,
  margin: 10,
  nav: false,
  dots: false,
  responsive: {
    0: {
      items: 3,
    },
    600: {
      items: 5,
    },
  },
});
const counters = document.querySelectorAll(".count");
const speed = 200;

counters.forEach((counter) => {
  const updateCount = () => {
    const target = parseInt(counter.getAttribute("data-target"));
    const count = parseInt(counter.innerText);
    const increment = Math.trunc(target / speed);

    if (count < target) {
      counter.innerText = count + increment;
      setTimeout(updateCount, 1);
    } else {
      counter.innerText = target;
    }
  };
  updateCount();
});

//back to top
const showOnPx = 100;
const backToTopButton = document.querySelector(".btn-fixed");

const scrollContainer = () => {
  return document.documentElement || document.body;
};
document.addEventListener("scroll", () => {
  if (scrollContainer().scrollTop > showOnPx) {
    backToTopButton.classList.remove("hidden");
  } else {
    backToTopButton.classList.add("hidden");
  }
});

//progress bar
const pageProgressBar = document.querySelector(".progress-bar");

document.addEventListener("scroll", () => {
  const scrolledPercentage =
    (scrollContainer().scrollTop /
      (scrollContainer().scrollHeight - scrollContainer().clientHeight)) *
    100;
  pageProgressBar.style.width = `${scrolledPercentage}%`;
  if (scrollContainer().scrollTop > showOnPx) {
    backToTopButton.classList.remove("hidden");
  } else {
    backToTopButton.classList.add("hidden");
  }
});
